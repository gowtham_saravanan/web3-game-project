import { useContext } from "react";
import { GameContext } from "../../context/GameContext";

function Header() {
    const {balance, currentAccount, membershipTokenId, gemBalance, connectToWallet} = useContext(GameContext);

    return (  
        <div className="pt-4 p-2 md:grid grid-cols-8 justify-center items-center">
            <div className="flex flex-col items-center ml-3 md:ml-0 col-span-3">
                <div className="flex items-center">
                    <span className="text-3xl text-blue-800">NFT Palace</span>
                </div>
                <p className="text-lg pt-2"> We sponser the gas for your transactions </p>
            </div>

            {/* <div className="flex justify-center items-center col-span-2 invisible md:visible">
                <a href="" target="_blank"><span className="mx-3 font-medium underline">Upgrade NFT</span></a>
            </div>   */}

            {
                currentAccount == '0x' && 
                <>
                    <button className="btn btn-blue text-center md:col-start-6 col-span-2 md:w-fit"
                    onClick={connectToWallet}>
                            {'Connect Wallet'}
                    </button>
                </>
            }

            {
                currentAccount != '0x' && 
                <>
                    <div className="flex m-3 p-2 rounded-3xl bg-white justify-center items-center text-sm md:col-start-5 col-span-2 md:w-fit">
                        <span className="rounded-full w-2 h-2 bg-green-400 mx-1"></span>
                        <span className="mx-1">{Number(balance).toFixed(4)} Goerli ETH</span>
                        <span className="bg-red-100 p-2 rounded-3xl mx-1">{currentAccount.slice(0, 5) + '....' + currentAccount.slice(currentAccount.length-5, currentAccount.length)}</span> 
                    </div>
                    <div className="flex m-3 p-2 rounded-3xl bg-white justify-center items-center text-sm col-span-2 md:w-fit">
                        <span className="rounded-full w-2 h-2 bg-green-400 mx-1"></span>
                        <span className="mx-1">{Number(gemBalance).toFixed(10)} Gems</span>
                    </div>
                </>
            }
            

            {/* <div className="flex justify-center items-center col-span-2 visible md:invisible">
                <a href="#upgrade_nft" target="_blank"><span className="mx-3 font-medium underline">Upgrade NFT</span></a>
            </div> */}
        </div>
    );
}

export default Header;