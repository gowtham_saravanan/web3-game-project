import { useContext, useState, useEffect } from "react";
import { listNFT } from "../../actions/Marketplace";
import { upgradeNFT } from "../../actions/NftV2";
import { GameContext } from "../../context/GameContext"; 

function UpgradeNFT() {
    const [tokenId, setTokenId] = useState('');
    const context = useContext(GameContext);
    
    const submit = (e) => {
        e.preventDefault();
        upgradeNFT({context, tokenId})
    }

    useEffect(() => {
        switch(context.upgradeNftStatus.status) {
            case "completed" : 
                setTokenId('');
                context.setUpgradeNftStatus({status: 'pending'});
                break;

        }
    }, [context.upgradeNftStatus])
    

    return (  
       <div className="flex justify-center mx-5 md:mx-8 pb-10" id="upgrade_nft">
           <div className="bg-white w-full md:w-2/3 p-5 shadow-md rounded-xl">
                <h1 className="pb-4 text-lg font-bold text-center">Upgrade NFT to ERC-1155</h1>
                <form onSubmit={(e) => submit(e)} className="grid grid-cols-6 gap-4 items-end">
                    <div className="col-span-6 sm:col-span-4">
                        <label className="input-label">Token Id</label>
                        <input type="text" id="name" className="input-field" placeholder="Token Id to be Upgraded   " required
                        value={tokenId} onChange={(e) => setTokenId(e.target.value)}/>
                    </div>
                    <button type="submit" className="btn btn-blue h-10 col-span-6 sm:col-span-2" disabled={context.upgradeNftStatus.loading}>
                        {context.upgradeNftStatus.loading ? 'Upgrading...' : 'Upgrade NFT'}
                    </button>
                </form>
           </div>
       </div>
    );
}

export default UpgradeNFT;