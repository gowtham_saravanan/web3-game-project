import { useContext, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClose, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons'
import { buyNFT, getListings } from "../../actions/Marketplace";
import { GameContext } from "../../context/GameContext";
import { ethers } from "ethers";

function Listing({listing}) {
    const context = useContext(GameContext);

    const tokenTypes = ["ERC-721", "ERC-1155"];
    const statusColors = ['bg-green-100', 'bg-red-300'];

    useEffect(() => {
        console.log(listing);
    }, [])

    const onBuy = (e) => {
        e.preventDefault();
        buyNFT({context, listing});
    }

    return(
        <div className='w-full sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/5 mx-5 my-2 sm:mx-2 lg:m-5 bg-white p-4 rounded-xl shadow-md flex flex-col justify-between'>
        <h5 className='text-center'>
            <span className="font-medium">Token ID - </span> {listing.tokenId.toString()}
        </h5>
        <div className='text-center'>
            <span className="font-medium"> Listing Price - </span>{ethers.utils.formatEther(listing.saleAmount)} Gems   
        </div>
        <div className='text-center'>
            <span className="font-medium">Token Type - </span>{tokenTypes[listing.tokenType]}   
        </div>
        <div className='text-center'>
            <span className="font-medium">Seller - </span>{listing.seller.slice(0, 5) + '....' + listing.seller.slice(listing.seller.length-5, listing.seller.length)}   
        </div>
        {
            listing.isCompleted && <div className='text-center mt-5'>
                <span className={"px-3 text-sm py-1 rounded-xl bg-green-300"}>
                    Sold to - {listing.buyer.slice(0, 5) + '....' + listing.buyer.slice(listing.buyer.length-5, listing.seller.length)}   
                </span>
            </div>
        }
        <div className="grid grid-cols-6 mt-4 items-center">
            <div className='col-span-3 text-center'>
                <span className={"px-3 text-sm py-1 rounded-xl " + statusColors[listing.isCompleted ? 1 : 0]}>
                    {listing.isCompleted ? "Ended" : "Active"}
                </span>
            </div>
            <div className='col-span-3 text-center'>
                <form onSubmit={(e) => onBuy(e)}>
                    <button type="submit" className="btn btn-blue text-center" 
                    disabled={
                        context.buyListingStatus.loading ||
                        listing.seller.toLowerCase() == context.currentAccount.toLowerCase() ||
                        listing.isCompleted
                        }>
                            {context.buyListingStatus.loading && (context.buyListingStatus.id == listing.id.toNumber()) ? 'Buying...' : 'Buy NFT'}
                    </button>
                </form>
            </div>    
        </div>
    </div>
    );
}

export default Listing;