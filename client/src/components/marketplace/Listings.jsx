import { useEffect, useContext } from "react";
import { getGemBalance } from "../../actions/Gem";
import { getListings } from "../../actions/Marketplace";
import { GameContext } from "../../context/GameContext";
import Listing from "./Listing";

function Listings() {
    const context = useContext(GameContext);

    useEffect(() => {
        getListings(context);
        getGemBalance(context);
    }, [context.currentAccount, context.network])
    
    return ( 
        <div className="pb-5 flex flex-wrap justify-center">
            {
                context.listings && context.listings.map((listing, index) => <Listing listing={listing} key={index}/>)
            }
        </div>
    );
}

export default Listings;