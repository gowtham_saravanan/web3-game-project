import {ethers} from 'ethers';
import { useContext, useState, useEffect } from "react";
import { listNFT } from "../../actions/Marketplace";
import { GameContext } from "../../context/GameContext"; 

function CreateListing() {
    const [tokenType, setTokenType] = useState(0);
    const [tokenId, setTokenId] = useState('');
    const [salePrice, setSalePrice] = useState(0);
    const [buyers, setBuyers] = useState('');
    const context = useContext(GameContext);
    
    const submit = (e) => {
        e.preventDefault();

        let buyersArray = buyers.split(",");
        let parsedSalePrice = ethers.utils.parseUnits(salePrice.toString(), "ether");
        listNFT({context, tokenType, tokenId, salePrice:parsedSalePrice, buyers:buyersArray})
    }

    useEffect(() => {
        switch(context.setListNFTStatus.status) {
            case "completed" : 
                setTask('');
                context.setListNFTStatus({status: 'pending'});
                break;
            
            case "failed": 
                context.setListNFTStatus({status: 'pending'});
                break;

        }
    }, [context.listNFTStatus])
    

    return (  
       <div className="flex justify-center mx-5 my-2 md:m-8">
           <div className="bg-white w-full md:w-2/3 p-5 shadow-md rounded-xl">
                <h1 className="pb-4 text-lg font-bold text-center">List your NFT for Sale</h1>
                <form onSubmit={(e) => submit(e)} className="grid grid-cols-6 gap-4 items-end">
                    <div className="col-span-3 sm:col-span-2">
                        <label className="input-label">Token Type</label>
                        <select id="status" className="input-field col-span-1 mt-4" placeholder="Token Type" required 
                        onChange={(e) => setTokenType(e.target.value)} value={tokenType}>
                            <option value="0">ERC-721</option>    
                            <option value="1">ERC-1155</option>    
                        </select>
                    </div>
                    <div className="col-span-3 sm:col-span-2">
                        <label className="input-label">Token Id</label>
                        <input type="text" id="name" className="input-field" placeholder="Token Id to be Listed" required
                        value={tokenId} onChange={(e) => setTokenId(e.target.value)}/>
                    </div>
                    <div className="col-span-3 sm:col-span-2">
                        <label className="input-label">Sale Price</label>
                        <input type="text" id="name" className="input-field" placeholder="Sale price" required
                        value={salePrice} onChange={(e) => setSalePrice(e.target.value)}/>
                    </div>
                    <div className="col-span-3 sm:col-span-4">
                        <label className="input-label">Buyers</label>
                        <input type="text" id="name" className="input-field" placeholder="Buyers address separated by comma" required
                        value={buyers} onChange={(e) => setBuyers(e.target.value)}/>
                    </div>
                    <button type="submit" className="btn btn-blue h-10 col-span-6 sm:col-span-2" disabled={context.listNFTStatus.loading}>
                        {context.listNFTStatus.loading ? 'Adding...' : 'List NFT for Sale'}
                    </button>
                </form>
           </div>
       </div>
    );
}

export default CreateListing;