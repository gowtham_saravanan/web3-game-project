import { gameV1ABI, gameV1Address, forwarderAddress } from "../utils/helper";
import {ethers} from 'ethers';

const getNftV1Contract = (context) => {
    return new ethers.Contract(gameV1Address, gameV1ABI , context.getProvider().getSigner());
}

const getForwarderContract = (context) => {
    return new ethers.Contract(forwarderAddress, forwarderABI , context.getProvider().getSigner());
}

export const getOwnerV1 = async ({context, tokenId}) => {
    return await getNftV1Contract(context).ownerOf(tokenId);
}

export const hasApprovalForV1 = async ({context, to, tokenId}) => {
    let approvedUser = await getNftV1Contract(context).getApproved(tokenId);
    return approvedUser.toLowerCase() == to.toLowerCase();
}


export const getApprovalForV1 = async ({context, to, tokenId}) => {
    let txn = await getNftV1Contract(context).approve(to, tokenId);
    await txn.wait();
    
    return await hasApprovalForV1({context, to, tokenId})
}