import axios from "axios"

export const relayTransaction = async ({request, signature}) => {
    return await axios.post("https://nft-marketplace-ten-tau.vercel.app/api/relay", {request,signature})
}