import { marketplaceABI, marketplaceAddress, forwarderABI, forwarderAddress } from "../utils/helper";
import {ethers} from 'ethers';
import { signMetaTxRequest } from "../utils/metatransactions";
import { relayTransaction } from "./Relay";
import { hasApprovalForV1, getApprovalForV1, getOwnerV1 } from "./NftV1";
import { getApprovalForGem, hasApprovalForGem } from "./Gem";

const getMarketplaceContract = (context) => {
    return new ethers.Contract(marketplaceAddress, marketplaceABI , context.getProvider().getSigner());
}

const getForwarderContract = (context) => {
    return new ethers.Contract(forwarderAddress, forwarderABI , context.getProvider().getSigner());
}

export const isWhitelistedBuyer = async ({context, listingId}) => {
    return await getMarketplaceContract(context).isWhitelistedBuyer(listingId, context.currentAccount);
}

export const getListings = async (context) => {
    try{
        await context.setListings(await getMarketplaceContract(context).getListings(1, 20));
    } catch(e) {
        context.setAlert({show: true, 'message' : e.reason})
        console.log(e.reason);
    }
}

export const listNFT = async ({context, tokenType, tokenId, salePrice, buyers}) => {
    console.log(salePrice);
    try{
        context.setListNFTStatus({status: 'pending', 'loading': true});

        let owner = await getOwnerV1({context, tokenId });
        if(owner.toLowerCase() != context.currentAccount.toLowerCase()){
            context.setAlert({show: true, 'message' : "You don't own this NFT"})
            context.setListNFTStatus({status: 'pending', 'loading': false});
            return;
        }
    
        let nftApproval = await hasApprovalForV1({context, to:marketplaceAddress, tokenId});
        if(!nftApproval){
            nftApproval = await getApprovalForV1({context, to:marketplaceAddress, tokenId})
            if(!nftApproval){
                context.setAlert({show: true, 'message' : "Approval to marketplace denied"})
                context.setListNFTStatus({status: 'pending', 'loading': false});
                return;
            }
        }
    
        let data = getMarketplaceContract(context).interface.encodeFunctionData('listNFT',
         [
            tokenType,
            tokenId,
            salePrice,
            buyers
        ]);
        let result = await signMetaTxRequest(context.getProvider().getSigner(), getForwarderContract(context), {
          to: getMarketplaceContract(context).address, from: context.currentAccount, data
        });
        console.log(result);
    
        let {request, signature} = result
       
        await relayTransaction({request, signature});

        await getListings(context);
        context.setListNFTStatus({status: 'completed', 'loading': false});

    } catch (error) {
        console.log(error);
        context.setAlert({show: true, 'message' : error.reason})
        context.setListNFTStatus({status: 'pending', 'loading': false});
    }

    
}

export const buyNFT = async ({context, listing}) => {

    try{
        let {id:listingId, tokenId, tokenType, saleAmount:amount} = listing;

        context.setBuyListingStatus({status: 'pending', 'loading': true, 'id': listingId});
    
        let isWhitelisted = await isWhitelistedBuyer({context, listingId});
        if(!isWhitelisted){
            context.setAlert({show: true, 'message' : "You are not whitelisted to buy this NFT"})
            context.setBuyListingStatus({status: 'pending', 'loading': false, 'id': 0});
            return;
        }
    
        let gemApproval = await hasApprovalForGem({context, to:marketplaceAddress, amount});
        if(!gemApproval){
            gemApproval = await getApprovalForGem({context, to:marketplaceAddress, amount})
            if(!gemApproval){
                context.setAlert({show: true, 'message' : "Approval to gems denied"})
                context.setBuyListingStatus({status: 'pending', 'loading': false, 'id': 0});
                return;
            }
        }
    
        let data = getMarketplaceContract(context).interface.encodeFunctionData('buyListing',
         [
            listingId
        ]);
        let result = await signMetaTxRequest(context.getProvider().getSigner(), getForwarderContract(context), {
          to: getMarketplaceContract(context).address, from: context.currentAccount, data
        });
        console.log(result);
    
        let {request, signature} = result
        await relayTransaction({request, signature});
        
        await getListings(context);
        context.setBuyListingStatus({status: 'completed', 'loading': false, 'id': listingId});
    } catch(error) {
        context.setAlert({show: true, 'message' : error.message})
        context.setBuyListingStatus({status: 'pending', 'loading': false, 'id': 0});
    }

    
}