import { gameV2ABI, gameV2Address, forwarderAddress, forwarderABI } from "../utils/helper";
import {ethers} from 'ethers';
import { getApprovalForV1, getOwnerV1, hasApprovalForV1 } from "./NftV1";
import { signMetaTxRequest } from "../utils/metatransactions";
import { relayTransaction } from "./Relay";
import { getApprovalForGem, getGemBalance, hasApprovalForGem } from "./Gem";

const getNftV2Contract = (context) => {
    return new ethers.Contract(gameV2Address, gameV2ABI , context.getProvider().getSigner());
}

const getForwarderContract = (context) => {
    return new ethers.Contract(forwarderAddress, forwarderABI , context.getProvider().getSigner());
}

export const upgradeNFT = async ({context, tokenId}) => {
    try{
        context.setUpgradeNftStatus({status: 'pending', 'loading': true});

        let owner = await getOwnerV1({context, tokenId });
        if(owner.toLowerCase() != context.currentAccount.toLowerCase()){
            context.setUpgradeNftStatus({status: 'pending', 'loading': false});
            context.setAlert({show: true, 'message' : "You don't own this NFT"})
            return;
        }

        let gemsNeeded = await getNftV2Contract(context).gemForUpgrade();
        if(ethers.utils.parseEther(context.gemBalance).lt(gemsNeeded)){
            context.setUpgradeNftStatus({status: 'pending', 'loading': false});
            context.setAlert({show: true, 'message' : "2 GEM's needed to upgrade"})
            return;  
        }
    
        let nftApproval = await hasApprovalForV1({context, to:gameV2Address, tokenId});
        if(!nftApproval){
            nftApproval = await getApprovalForV1({context, to:gameV2Address, tokenId})
            if(!nftApproval){
                context.setUpgradeNftStatus({status: 'pending', 'loading': false});
                context.setAlert({show: true, 'message' : "Approval of V1 NFT denied"})
                return;
            }
        }
    
        let gemApproval = await hasApprovalForGem({context, to:gameV2Address, amount:gemsNeeded});
        if(!gemApproval){
            gemApproval = await getApprovalForGem({context, to:gameV2Address, amount:gemsNeeded})
            if(!gemApproval){
                context.setUpgradeNftStatus({status: 'pending', 'loading': false});
                context.setAlert({show: true, 'message' : "Approval to gems denied"})
                return;
            }
        }
    
        let data = getNftV2Contract(context).interface.encodeFunctionData('upgradeFromV1toV2', [tokenId]);
        let result = await signMetaTxRequest(context.getProvider().getSigner(), getForwarderContract(context), {
          to: getNftV2Contract(context).address, from: context.currentAccount, data
        });
        console.log(result);
    
        let {request, signature} = result
        await relayTransaction({request, signature});

        await getGemBalance(context);
        context.setUpgradeNftStatus({status: 'completed', 'loading': false});
        context.setAlert({show: true, 'message' : "Upgrade completed!!!"})
    }catch(e){
        console.log(e);
        context.setAlert({show: true, 'message' : e.reason})
        context.setUpgradeNftStatus({status: 'pending', 'loading': false});
    }
}