import { gemABI, gemAddress, forwarderAddress } from "../utils/helper";
import {ethers} from 'ethers';

const getGemContract = (context) => {
    return new ethers.Contract(gemAddress, gemABI , context.getProvider().getSigner());
}

const getForwarderContract = (context) => {
    return new ethers.Contract(forwarderAddress, forwarderABI , context.getProvider().getSigner());
}

export const getGemBalance = async (context) => {
    context.setGemBalance(ethers.utils.formatEther(await getGemContract(context).balanceOf(context.currentAccount)));
}

export const hasApprovalForGem = async ({context, to, amount}) => {
    let allowance = await getGemContract(context).allowance(context.currentAccount, to);
    return allowance.gte(amount);
}

export const getApprovalForGem = async ({context, to, amount}) => {
    let txn = await getGemContract(context).approve(to, amount);
    await txn.wait();
    
    return await hasApprovalForGem({context, to, amount})
}