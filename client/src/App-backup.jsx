import { useState, useEffect } from 'react'
import {ethers} from 'ethers';
import { forwarderABI, gemABI, gameV1ABI, gameV2ABI, marketplaceABI } from './utils/helper';
import './css/App.css'
import { signMetaTxRequest } from './utils/metatransactions';
import axios from "axios";

function App() {
  const {ethereum} = window;
  const provider = new ethers.providers.Web3Provider(ethereum);
  const [currentAccount, setCurrentAccount] = useState('0x');
  const [balance, setBalance] = useState(0);
  const [loading, setLoading] = useState(false);
  const [v1NFTBalance, setv1NFTBalance] = useState(0);
  const [v2NFTBalance, setv2NFTBalance] = useState(0);
  const [gems, setGems] = useState(0)
  const [receiverGems, setReceiverGems] = useState(0)

  const getConnectedAccount = async () => {
    console.log("1");
    const accounts = await ethereum.request({method: 'eth_accounts'})
    setCurrentAccount(accounts.length ? accounts[0] : '0x'); 
  }
  
  const connectToWallet = async () => {
      try{
          const accounts = await ethereum.request({ method : 'eth_requestAccounts' })
          setCurrentAccount(accounts.length ? accounts[0] : '0x');
      } catch (error){
        alert(error.message);
      }
  }
 
  const getBalance = async () => {
    const balance = await provider.getBalance(currentAccount);
    setBalance(Math.round(ethers.utils.formatEther(balance) * 1e4) / 1e4);
  }

  const getForwarderContract = () => {
    return new ethers.Contract("0x294D51C1d672B010c7647F7D89262747c1148910", forwarderABI , provider.getSigner());
  }

  const getGemContract = () => {
    return new ethers.Contract("0x4b6b0e248a42989edC3f2A82aa655B8a923923f3", gemABI , provider.getSigner());
  }
  
  const getV1NftContract = () => {
    return new ethers.Contract("0x2272a70eeD649380e2f77f46aDA65a1B9400dBe8", gameV1ABI , provider.getSigner());
  }

  const getV2NftContract = () => {
    return new ethers.Contract("0xd242A992Da612295558dD495967bb09955DcB7eb", gameV2ABI , provider.getSigner());
  }

  const getMarketplaceContract = () => {
    return new ethers.Contract("0x16Deb6e378C903c53866e6f849c101e8AA2b519a", marketplaceABI , provider.getSigner());
  }

  const getGemsBalance = async () => {
    const balance = await getGemContract().balanceOf(currentAccount);
    setGems(ethers.utils.formatUnits(balance, 0));
  }

  const getReceiverGemsBalance = async () => {
    const balance = await getGemContract().balanceOf("0xeD3836DA347E04e570908E868743e3Ba393f847E");
    setReceiverGems(ethers.utils.formatUnits(balance, 0));
  }

  const getV1NftBalance = async () => {
    const balance = await getV1NftContract().balanceOf(currentAccount);
    setv1NFTBalance(ethers.utils.formatUnits(balance, 0));
  }

  const transferGem = async () => {
    setLoading(true);
    let data = getGemContract().interface.encodeFunctionData('transfer', ["0xeD3836DA347E04e570908E868743e3Ba393f847E", ethers.utils.parseUnits("100", 0)]);
    let result = await signMetaTxRequest(provider.getSigner(), getForwarderContract(), {
      to: getGemContract().address, from: currentAccount, data
    });
    console.log(result);
    axios
    .post("http://127.0.0.1:3000/api/relay", {
      request: result.request,
      signature: result.signature
    })
    .then((response) => {
      getGemsBalance();
      getReceiverGemsBalance();
      console.log(response);
      setLoading(false);
    });
    
  }
    
  useEffect(() => {
    if(!ethereum) return;
      getConnectedAccount();
  }, [])

  useEffect(() => {
      if(currentAccount != '0x'){
         getBalance();
         getGemsBalance();
         getV1NftBalance();
         getReceiverGemsBalance();
      }
  }, [currentAccount])

  return (
    <div className="App">
      
      <h1>Game Project Demo</h1>
      <div className="card">
        {
          currentAccount == '0x' &&
          <button onClick={() => connectToWallet()}>
          Connect Wallet
          </button>
        }
        {
          currentAccount != '0x' &&
          <div>
            <p><code>Account Connected - {currentAccount}</code></p>
            <p><code>ETH Balance - {balance} GoerliETH</code></p>          
            <p><code>GEM Tokens - {gems}</code></p>                    
          </div>
        }

        {
           currentAccount != '0x' && <button onClick={() => transferGem()}>
            {loading ? "Loading ..." :  "Transfer 100 Gems"}
           </button>
        }

        {
          currentAccount != '0x' && <div>
            <p><code>Receiver GEM Tokens balance - {receiverGems}</code></p>                    
          </div>
        }
        
      </div>
    </div>
  )
}

export default App
