import { createContext, useState, useEffect } from "react";
import {ethers} from 'ethers';

export const GameContext = createContext();
const {ethereum} = window;

export const GameContextProvider = ({children}) => {

    const [currentAccount, setCurrentAccount] = useState('0x');
    const [listings, setListings] = useState([]);  
    const [balance, setBalance] = useState(0);  
    const [gemBalance, setGemBalance] = useState(0);  
    const [network, setNetwork] = useState({});  
    const [isMember, setIsMember] = useState(false);  
    const [membershipPrice, setMembershipPrice] = useState("0 ether");  
    const [membershipTokenId, setMembershipTokenId] = useState(-1);  
    const [listNFTStatus, setListNFTStatus] = useState({
        'loading' : false,
        'status' : 'pending'
    });
    const [buyListingStatus, setBuyListingStatus] = useState({
        'id' : '0',
        'loading' : false,
        'status' : 'pending'
    });
    const [upgradeNftStatus, setUpgradeNftStatus] = useState({
        'loading' : false,
        'status' : 'pending'
    });
    const [alert, setAlert] = useState({
        'show' : false,
        'message' : '',
        'status' : '',
    })

    const getProvider = () => {
        if(!ethereum) return;
        return new ethers.providers.Web3Provider(ethereum);
    }
    
    const getConnectedAccount = async () => {
        const accounts = await ethereum.request({method: 'eth_accounts'})
        setCurrentAccount(accounts.length ? accounts[0] : '0x');
    }
    
    const connectToWallet = async () => {
        try{
            const accounts = await ethereum.request({ method : 'eth_requestAccounts' })
            setCurrentAccount(accounts.length ? accounts[0] : '0x');
            setAlert({show: true, 'message' : 'Wallet connected successfully'})
        }catch (error){
            setAlert({show: true, 'message' : (error.code == 4001 ? "Connect wallet to continue" : error.message)})
        }
    }

    const getNetwork = async () => {
        const network = await getProvider().getNetwork();
        setNetwork(network);
    }
    
    const isWalletConnected = () => {
        return (currentAccount != '0x' && (network.chainId && network.chainId == 5));
    } 
    
    const getBalance = async () => {
        const balance = await getProvider().getBalance(currentAccount);
        setBalance(ethers.utils.formatEther(balance));
    }
  
    useEffect(() => {
        if(!ethereum) return;

        getNetwork();
        ethereum.on('accountsChanged', getConnectedAccount);
        ethereum.on('chainChanged', () => {
            window.location.reload()
        })
        getConnectedAccount()    
        
        return () => {
            if(ethereum) ethereum.off('accountsChanged', getConnectedAccount);
          }
    }, [])

    useEffect(() => {
        if(!ethereum) return;
        
        if(!isWalletConnected()){
            setListings([]);
            setBalance(0);
            return;
        }

        getBalance();
    }, [currentAccount, network])

    
    return (
        <GameContext.Provider value={{connectToWallet, currentAccount, balance, listings, network, listNFTStatus,
         buyListingStatus, setBuyListingStatus, setAlert, alert,
         setListNFTStatus, getProvider, setListings, setNetwork, setBalance, isWalletConnected, isMember, setIsMember, 
         upgradeNftStatus, setUpgradeNftStatus, membershipPrice, setMembershipPrice, membershipTokenId, setMembershipTokenId,
         gemBalance, setGemBalance}}>
            {children}
        </GameContext.Provider>
    );
}