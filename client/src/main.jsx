import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './css/index.css'
import { GameContextProvider } from './context/GameContext'

ReactDOM.createRoot(document.getElementById('root')).render(
  <GameContextProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </GameContextProvider>,
)
