import { useState, useEffect } from 'react'
import Alert from './components/common/Alert';
import Header from './components/common/Header';
import CreateListing from './components/marketplace/CreateListing';
import Listings from './components/marketplace/Listings';
import UpgradeNFT from './components/marketplace/UpgradeNFT';

function App() {

  return <div className="w-full min-h-screen bg-slate-100">
    <Alert/>
    <Header/>
    <CreateListing/>
    <Listings/>
    <UpgradeNFT/>
  </div>

}

export default App;
