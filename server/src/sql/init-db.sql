CREATE DATABASE web3_game;

USE web3_game;

CREATE TABLE relay_log (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	fromAddress VARCHAR(50) NOT NULL,
    toAddress VARCHAR(50) NOT NULL,
    gasPrice BIGINT NOT NULL,
    gasUsed BIGINT NOT NULL,
    totalGas BIGINT NOT NULL,
    transactionHash VARCHAR(70) NOT NULL 
);