const { ethers } = require("ethers");
const provider = new ethers.providers.AlchemyProvider("goerli", process.env.ALCHEMY_KEY)
const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
const {forwarderABI} = require("../helper"); 
const forwarder = new ethers.Contract("0x3A0a82eD7e4a769c1D0d52B0fcd80D374f58Adb4", forwarderABI, signer) 
const RelayLog = require('../models/relay_log');

const postTransaction = async (req, res) => {
    try {
        let txn = await forwarder.execute(req.body.request, req.body.signature);
        let data = await txn.wait();

        const relayLog = new RelayLog(
            data.from,
            data.to,
            data.effectiveGasPrice.toNumber(),
            data.gasUsed.toNumber(),
            data.effectiveGasPrice.toNumber() * data.gasUsed.toNumber(),
            data.transactionHash
        );
        await relayLog.save();

        res.send({
            statusCode: 200,
            statusMessage: 'Ok',
            message: 'Transaction completed',
            data,
        });
    } catch (err) {
        res.status(500).send({ statusCode: 500, statusMessage: 'Internal Server Error', message: null, data: err });
    }
};

module.exports = {
    postTransaction
};
