const pool = require('../databases/mysql.db');

class RelayLog {
    constructor(fromAddress, toAddress, gasPrice, gasUsed, totalGas, transactionHash) {
        this._fromAddress = fromAddress;
        this._toAddress = toAddress;
        this._gasPrice = gasPrice;
        this._gasUsed = gasUsed;
        this._totalGas = totalGas;
        this._transactionHash = transactionHash;
    }
    
    async save() {
        const sql = `INSERT INTO relay_log (fromAddress, toAddress, gasPrice, gasUsed, totalGas, transactionHash) VALUES ("${this._fromAddress}", "${this._toAddress}", "${this._gasPrice}", "${this._gasUsed}", "${this._totalGas}", "${this._transactionHash}")`;
        await pool.execute(sql);
    }
}

module.exports = RelayLog;
