const express = require('express');

const apiController = require('../controllers/api.controller');

const router = express.Router();

// Endpoint for getting all the records
router.post('/relay', apiController.postTransaction);

module.exports = router;
