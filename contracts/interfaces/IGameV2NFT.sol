// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";

interface IGameV2NFT is IERC1155 {

    function tokenCreatedAt(uint token) external returns(uint);

    function upgradeFromV1toV2(uint id) external;

}