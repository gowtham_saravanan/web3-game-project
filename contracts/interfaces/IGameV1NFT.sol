// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

interface IGameV1NFT is IERC721 {

    function safeMint(address to) external;
    function burn(uint tokenId) external;

}