// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import './interfaces/IGameV1NFT.sol';
import './interfaces/IGameGem.sol';

contract GameV2NFT is ERC2771Context, ERC1155, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIdCounter;
    
    IGameV1NFT public immutable nftV1;
    IGemToken public immutable token;
    uint public gemForUpgrade;
    uint holdTime = 7 days;
    mapping(uint => uint) public tokenCreatedAt;

    constructor(address _nftV1, address _token, uint _gemForUpgrade, address _trustedForwarder) ERC2771Context(_trustedForwarder) ERC1155("") {
        nftV1 = IGameV1NFT(_nftV1);
        token = IGemToken(_token);
        gemForUpgrade = _gemForUpgrade;
    }

    function upgradeFromV1toV2(uint _tokenIdV1) public {
        uint256 tokenId = _tokenIdCounter.current();
        
        token.transferFrom(_msgSender(), address(this), gemForUpgrade);
        nftV1.burn(_tokenIdV1);

        _mint(_msgSender(), tokenId, 1, "");
        tokenCreatedAt[tokenId] = block.timestamp;
        _tokenIdCounter.increment();
    }

    function safeTransferFrom(
        address from,
        address to,
        uint256 id,
        uint256 amount,
        bytes memory data
    ) public override {
        require(tokenCreatedAt[id] + holdTime < block.timestamp, "Need to wait for 7days to transfer token");
        require(
            from == _msgSender() || isApprovedForAll(from, _msgSender()),
            "ERC1155: caller is not token owner or approved"
        );
        _safeTransferFrom(from, to, id, amount, data);
    }

    function safeBatchTransferFrom(
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) public override {

        for(uint i = 0; i < ids.length; i++) {
            require(tokenCreatedAt[ids[i]] + holdTime < block.timestamp, "Need to wait for 7days to transfer token");
        }

        require(
            from == _msgSender() || isApprovedForAll(from, _msgSender()),
            "ERC1155: caller is not token owner or approved"
        );
        _safeBatchTransferFrom(from, to, ids, amounts, data);
    }

    function _msgSender() internal view override(Context, ERC2771Context)
      returns (address sender) {
      sender = ERC2771Context._msgSender();
    } 

    function _msgData() internal view override(Context, ERC2771Context)
        returns (bytes calldata) {
        return ERC2771Context._msgData();
    }
}