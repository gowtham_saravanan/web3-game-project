// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import './interfaces/IGameV1NFT.sol';
import './interfaces/IGameV2NFT.sol';
import './interfaces/IGameGem.sol';
import "@openzeppelin/contracts/utils/Counters.sol";
import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155Receiver.sol";
import "@openzeppelin/contracts/token/ERC721/utils/ERC721Holder.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "hardhat/console.sol";

contract GameMarketplace is ERC2771Context, ERC721Holder, ERC1155Holder {
    using Counters for Counters.Counter;
    
    enum TokenType {ERC721, ERC1155}
    struct Listing {
        uint id;
        TokenType tokenType;
        uint tokenId;
        address seller;
        address buyer;
        uint saleAmount;
        bool isCompleted;
    }
    
    mapping(uint => Listing) listings;
    mapping(uint => mapping(address => bool)) public isWhitelistedBuyer;
    Counters.Counter private listingIdCounter;

    IGameV1NFT public immutable nft1;
    IGameV2NFT public immutable nft2;
    IGemToken public immutable token;
    
    uint8 public fee;
    uint nftV2HoldTime = 7 days;
    address[] owners;
    mapping(address => bool) public isOwner;
    mapping(address => uint8) public getOwnership; 
    mapping(address => uint) public tokenBalanceOf;

    event NewListingCreated(TokenType tokenType, uint listingId, uint tokenId, uint saleAmount, address[] buyers);
    event ListingCompleted(uint listingId, uint tokenId, uint saleAmount);
    event TokenDeposited(uint listingId, address user, uint amount);

    constructor(
        address _nft1,
        address _nft2,
        address _token,
        uint8 _fee,
        address[] memory _owners,
        uint8[] memory _ownerships,
        address _trustedForwarder
    ) ERC2771Context(_trustedForwarder) {
        require(_nft1 != address(0) && _token != address(0), "Invalid tokens provided");
        require(_owners.length == _ownerships.length, "Invalid owners and ownerships length");

        nft1 = IGameV1NFT(_nft1);
        nft2 = IGameV2NFT(_nft2);
        token = IGemToken(_token);
        fee = _fee;

        for(uint i; i < _owners.length ; i++){
            owners.push(_owners[i]);
            isOwner[_owners[i]] = true;
            getOwnership[_owners[i]] = _ownerships[i];
        }
    }

    function getListings(uint page, uint perPage) external view returns(Listing[] memory) {
        require(page > 0 && perPage > 0, "Invalid input");
        uint start = ((page - 1) * perPage) + 1;
        uint lastListingId = listingIdCounter.current();
        require(lastListingId > 0 && lastListingId > (start - 1), "Not enough Listings");
        
        uint total = lastListingId - (start - 1);
        total = total > perPage ? perPage : total;
        Listing[] memory myLists = new Listing[](total);
        uint index = 0;
        for(uint i = start; i < (start + total); i++ ){
            myLists[index] = listings[i];
            ++index;
        }
        return myLists;
    }

    function getListing(uint _listingId) external view returns(uint, address, uint, bool, TokenType) {
        Listing memory listing = listings[_listingId];
        require(_listingId > 0 && listing.id == _listingId && listing.seller != address(0), "Listing doesn't exist");
        return (listing.tokenId, listing.seller, listing.saleAmount, listing.isCompleted, listing.tokenType);
    }

    function listNFT(TokenType _tokenType, uint _tokenId, uint _saleAmount, address[] memory _buyers) external returns(uint listingId) {
        require(_tokenType == TokenType.ERC721 || _tokenType == TokenType.ERC1155, "Invalid token type");

        listingIdCounter.increment();
        listingId = listingIdCounter.current();
        if(_tokenType == TokenType.ERC721){
            nft1.safeTransferFrom(_msgSender(), address(this), _tokenId);
        }
        if(_tokenType == TokenType.ERC1155){
            require(nft2.tokenCreatedAt(_tokenId) + nftV2HoldTime < block.timestamp, "You have to wait 1 week to transfer token");
            nft2.safeTransferFrom(_msgSender(), address(this), _tokenId, 1, "0x0");
        } 

        listings[listingId] = Listing(listingId, _tokenType, _tokenId, _msgSender(), address(0) , _saleAmount, false);   
        for(uint i; i < _buyers.length ; i++){
            isWhitelistedBuyer[listingId][_buyers[i]] = true;
        }

        emit NewListingCreated(_tokenType ,listingId, _tokenId, _saleAmount, _buyers);
    }

    function buyListing(uint _listingId) external {
        Listing memory listing = listings[_listingId];
        require(listing.seller != address(0), "Listing doesn't exist");
        require(!listing.isCompleted, "Listing already completed");
        
        token.transferFrom(_msgSender(), address(this), listing.saleAmount);
        
        // 95% to seller and 5% to marketplace
        uint sellerBalance = (listing.saleAmount * 95)/100;
        tokenBalanceOf[listing.seller] += sellerBalance;
        emit TokenDeposited(_listingId, listing.seller, sellerBalance); 

        uint platformFee = (listing.saleAmount * 5)/100;
        for(uint i; i < owners.length ; i++) {
            uint ownerBalance = (platformFee * getOwnership[owners[i]])/100;
            tokenBalanceOf[owners[i]] += ownerBalance; 
            emit TokenDeposited(_listingId, listing.seller, ownerBalance); 
        }

        if(listing.tokenType == TokenType.ERC721){
            nft1.safeTransferFrom(address(this), _msgSender(), listing.tokenId);          
        }
        if(listing.tokenType == TokenType.ERC1155){
            nft2.safeTransferFrom(address(this), _msgSender(), listing.tokenId, 1, "0x0");          
        }
        listings[_listingId].isCompleted = true;
        listings[_listingId].buyer = _msgSender();
        emit ListingCompleted(_listingId, listing.tokenId, listing.saleAmount);
    }

    function withdrawAllTokens() external {
        token.transfer(_msgSender(), tokenBalanceOf[_msgSender()]);
    }
}