// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IUniswapV2Router {
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);

    function swapTokensForExactTokens(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);
}

interface IWETH is IERC20 {
    function deposit() external payable;

    function withdraw(uint amount) external; 
}

contract UniswapV2Swap {

    address private constant UNISWAP_ROUTER = 0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D;
    address private constant FACTORY = 0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f;
    address private constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
    address private constant USDT = 0xdAC17F958D2ee523a2206206994597C13D831ec7;
    address private constant USDC = 0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48;
    
    IUniswapV2Router private router = IUniswapV2Router(UNISWAP_ROUTER);
    IERC20 private weth = IERC20(WETH);
    IERC20 private usdt = IERC20(USDT);
    IERC20 private usdc = IERC20(USDC);

    function singleSwapExactAmountIn(uint _amountIn, uint _minAmountOut) external returns(uint) {
        weth.transferFrom(msg.sender, address(this), _amountIn);
        weth.approve(address(router), _amountIn);

        address[] memory path = new address[](2);
        path[0] = WETH;
        path[1] = USDC;

        uint[] memory amounts = router.swapExactTokensForTokens(_amountIn, _minAmountOut, path, msg.sender, block.timestamp);

        return amounts[1];
    }

    function singleSwapExactAmountOut(uint _amountOut, uint _amountIn) external returns(uint) {
        usdc.transferFrom(msg.sender, address(this), _amountIn);
        usdc.approve(address(router), _amountIn);

        address[] memory path = new address[](2);
        path[0] = USDC;
        path[1] = USDT;

        uint[] memory amounts = router.swapTokensForExactTokens(_amountOut, _amountIn, path, msg.sender, block.timestamp);

        if(amounts[0] < _amountIn){
            usdc.transfer(msg.sender, _amountIn - amounts[0]);
        }

        return amounts[1];
    }

}