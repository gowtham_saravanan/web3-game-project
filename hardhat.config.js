require("@nomicfoundation/hardhat-toolbox");
require("@nomicfoundation/hardhat-network-helpers");
require("@nomiclabs/hardhat-etherscan");

/** @type import('hardhat/config').HardhatUserConfig */

module.exports = {
  solidity: "0.8.17",
  networks: {
    goerli: {
      url: 'https://eth-goerli.g.alchemy.com/v2/e0R4vnwcEs-_2NGslz9SPOQozOK-klh-',
      accounts: ['f20180b8d4d25b713ebd80bcd7bc52c9cf4ca5fdf7cd5a8bdb56a03cd3f24b3c']
    }
  },
  etherscan: {
    apiKey: '2VTDHYX7JU8JXZNAVI1SGSZEIRDPUPK2II'
  }
};
