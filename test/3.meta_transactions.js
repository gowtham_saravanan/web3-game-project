const {expect} = require("chai");
const {ethers} = require("hardhat");
const ethSigUtil = require('@metamask/eth-sig-util');
const { signMetaTxRequest } = require('./helpers/metatransactions');


describe("All Tokens Works", () => {
    let forwarderContract, test, treasury, user, nonce;
      
    beforeEach (async() => {
        [treasury, user] = await ethers.getSigners();

        let Forwarder = await ethers.getContractFactory("GameMetaForwarder");
        forwarderContract = await Forwarder.deploy();

        let Test = await ethers.getContractFactory("GameTest");
        test = await Test.deploy(forwarderContract.address);
    });

    it("Test", async () => {
        const from = user.address;
        const data = test.interface.encodeFunctionData('increment', [3940]);
        const result = await signMetaTxRequest(user, forwarderContract, {
            to: test.address, from, data
        });

        await forwarderContract.updateWhiteList(test.address, true);
        await forwarderContract.execute(result.request, result.signature);

        console.log(await test.data());
    });
});