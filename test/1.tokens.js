const {expect} = require("chai");
const {ethers} = require("hardhat");

describe("All Tokens Works", () => {
    let gameV1NFT, gems, gameV2NFT;
    
    beforeEach (async() => {
        [account1, account2, account3] = await ethers.getSigners();

        let GameV1NFT = await ethers.getContractFactory("GameV1NFT");
        gameV1NFT = await GameV1NFT.deploy();

        let GameGems = await ethers.getContractFactory("GameGems");
        gems = await GameGems.deploy();

        let GameV2NFT = await ethers.getContractFactory("GameV2NFT");
        gameV2NFT = await GameV2NFT.deploy(gameV1NFT.address, gems.address, ethers.utils.parseEther("1"));

    });

    it("Minting V1 NFT's work", async () => {
        expect(await gameV1NFT.balanceOf(account2.address)).equals(0);
        await gameV1NFT.safeMint(account2.address);
        expect(await gameV1NFT.balanceOf(account2.address)).equals(1);
        expect(await gameV1NFT.ownerOf(0)).equals(account2.address);
    });

    it("Minting Gem Token works", async () => {
        expect(await gems.balanceOf(account2.address)).equals(0);
        await gems.mint(account2.address, ethers.utils.parseEther("1"));
        expect(await gems.balanceOf(account2.address)).equals(ethers.utils.parseEther("1"));
    });

    it("Minting V2 NFT's work", async () => {
        await gameV1NFT.safeMint(account2.address);
        await gems.mint(account2.address, ethers.utils.parseEther("1"));
        expect(await gameV1NFT.balanceOf(account2.address)).equals(1);
        expect(await gameV1NFT.ownerOf(0)).equals(account2.address);
        expect(await gems.balanceOf(account2.address)).equals(ethers.utils.parseEther("1"));

        expect(await gameV2NFT.balanceOf(account2.address, 0)).equals(0);
        await gameV1NFT.connect(account2).approve(gameV2NFT.address, 0);
        await gems.connect(account2).approve(gameV2NFT.address, ethers.utils.parseEther("1"));
        await gameV2NFT.connect(account2).upgradeFromV1toV2(0);

        expect(await gameV2NFT.balanceOf(account2.address, 0)).equals(1);
        expect(await gameV1NFT.balanceOf(account2.address)).equals(0);
        expect(await gems.balanceOf(account2.address)).equals(ethers.utils.parseEther("0"));
        console.log(await gameV2NFT.tokenCreatedAt(0));
    });
});