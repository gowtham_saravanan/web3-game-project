const {expect} = require("chai");
const {ethers} = require("hardhat");
const {time} = require("@nomicfoundation/hardhat-network-helpers");
const { signMetaTxRequest } = require('./helpers/metatransactions');

describe("Game Marketplace", () => {
    let gameV1NFT, gameV2NFT, gems, marketplace, forwarder;
    
    beforeEach (async() => {
        [treasury, seller, buyer1, buyer2, owner1, owner2, owner3] = await ethers.getSigners();

        let Forwarder = await ethers.getContractFactory("GameMetaForwarder");
        forwarder = await Forwarder.deploy();

        let GameV1NFT = await ethers.getContractFactory("GameV1NFT");
        gameV1NFT = await GameV1NFT.deploy(forwarder.address);

        let GameGems = await ethers.getContractFactory("GameGems");
        gems = await GameGems.deploy(forwarder.address);

        let GameV2NFT = await ethers.getContractFactory("GameV2NFT");
        gameV2NFT = await GameV2NFT.deploy(gameV1NFT.address, gems.address, ethers.utils.parseEther("1"), forwarder.address);

        let Marketplace = await ethers.getContractFactory("GameMarketplace");
        marketplace = await Marketplace.deploy(
            gameV1NFT.address,
            gameV2NFT.address,
            gems.address,
            5,
            [owner1.address, owner2.address, owner3.address],
            [30, 35, 35],
            forwarder.address
        );

        await forwarder.whiteListContracts([gameV1NFT.address, gameV2NFT.address, gems.address, marketplace.address]);
    });

    async function sendMetaTransaction(signer, to, data){
        let result = await signMetaTxRequest(signer, forwarder, {
            to: to.address, from: signer.address, data
        });
        await forwarder.execute(result.request, result.signature);
    }

    it("Initialization works", async () => {
        expect(await marketplace.fee()).equals(5);
        expect(await marketplace.isOwner(owner1.address)).equals(true);
        expect(await marketplace.isOwner(owner2.address)).equals(true);
        expect(await marketplace.isOwner(owner3.address)).equals(true);
        expect(await marketplace.getOwnership(owner1.address)).equals(30);
        expect(await marketplace.getOwnership(owner2.address)).equals(35);
        expect(await marketplace.getOwnership(owner3.address)).equals(35);
    });

    it("List V1 NFT works", async () => {
        await gameV1NFT.safeMint(seller.address);
        expect(await gameV1NFT.ownerOf(0)).equals(seller.address);

        let data = gameV1NFT.interface.encodeFunctionData('approve', [marketplace.address, 0]);
        await sendMetaTransaction(seller, gameV1NFT, data);

        await marketplace.connect(seller).listNFT(0, 0, ethers.utils.parseEther("100"), [buyer1.address, buyer2.address])
        expect(await gameV1NFT.ownerOf(0)).equals(marketplace.address);

        expect(await marketplace.isWhitelistedBuyer(0, buyer1.address)).equals(true);
        expect(await marketplace.isWhitelistedBuyer(0, buyer2.address)).equals(true);
        expect(await marketplace.isWhitelistedBuyer(0, owner1.address)).equals(false);
        
        let [tokenId, sellerAddress, saleAmount, status, tokenType] = await marketplace.getListing(0);
        expect(tokenId).equals(0);
        expect(sellerAddress).equals(seller.address);
        expect(saleAmount).equals(ethers.utils.parseEther("100"));
        expect(status).equals(false);
        expect(tokenType).equals(0);
    });

    it("Buy V1 NFT works and withdraw amount", async () => {
        let saleAmount = ethers.utils.parseEther("100");
        await gameV1NFT.safeMint(seller.address);
        await gameV1NFT.connect(seller).approve(marketplace.address, 0);
        await marketplace.connect(seller).listNFT(0, 0, saleAmount, [buyer1.address, buyer2.address])
        
        await gems.mint(buyer1.address, saleAmount);
        await gems.connect(buyer1).approve(marketplace.address, saleAmount);
        await marketplace.connect(buyer1).buyListing(0);

        expect(await gameV1NFT.ownerOf(0)).equals(buyer1.address);
        let sellerBalance = saleAmount.mul(95).div(100);
        expect(await marketplace.tokenBalanceOf(seller.address)).equals(sellerBalance);
        let platformFee = saleAmount.mul(5).div(100);
        expect(await marketplace.tokenBalanceOf(owner1.address)).equals(platformFee.mul(30).div(100));
        expect(await marketplace.tokenBalanceOf(owner2.address)).equals(platformFee.mul(35).div(100));
        expect(await marketplace.tokenBalanceOf(owner3.address)).equals(platformFee.mul(35).div(100));

        expect(await gems.balanceOf(seller.address)).equals(0);
        await marketplace.connect(seller).withdrawAllTokens();
        expect(await gems.balanceOf(seller.address)).equals(sellerBalance);
    });

    it("List V2 NFT works", async () => {

        await gameV1NFT.safeMint(seller.address);
        await gems.mint(seller.address, ethers.utils.parseEther("1"));
        await gameV1NFT.connect(seller).approve(gameV2NFT.address, 0);
        await gems.connect(seller).approve(gameV2NFT.address, ethers.utils.parseEther("1"));
        await gameV2NFT.connect(seller).upgradeFromV1toV2(0);
        expect(await gameV2NFT.balanceOf(seller.address, 0)).equals(1);

        await time.increase(3600 * 24 * 8);
        await gameV2NFT.connect(seller).setApprovalForAll(marketplace.address, true);
        await marketplace.connect(seller).listNFT(1, 0, ethers.utils.parseEther("100"), [buyer1.address, buyer2.address])
        
        expect(await gameV2NFT.balanceOf(seller.address, 0)).equals(0);
        expect(await marketplace.isWhitelistedBuyer(0, buyer1.address)).equals(true);
        expect(await marketplace.isWhitelistedBuyer(0, buyer2.address)).equals(true);
        expect(await marketplace.isWhitelistedBuyer(0, owner1.address)).equals(false);
        
        let [tokenId, sellerAddress, saleAmount, status, tokenType] = await marketplace.getListing(0);
        expect(tokenId).equals(0);
        expect(sellerAddress).equals(seller.address);
        expect(saleAmount).equals(ethers.utils.parseEther("100"));
        expect(status).equals(false);
        expect(tokenType).equals(1);
    });


    it("Buy V1 NFT works and withdraw amount", async () => {
        let saleAmount = ethers.utils.parseEther("100");
        await gameV1NFT.safeMint(seller.address);
        await gems.mint(seller.address, ethers.utils.parseEther("1"));
        await gameV1NFT.connect(seller).approve(gameV2NFT.address, 0);
        await gems.connect(seller).approve(gameV2NFT.address, ethers.utils.parseEther("1"));
        await gameV2NFT.connect(seller).upgradeFromV1toV2(0);

        await time.increase(3600 * 24 * 8);
        await gameV2NFT.connect(seller).setApprovalForAll(marketplace.address, true);
        await marketplace.connect(seller).listNFT(1, 0, saleAmount, [buyer1.address, buyer2.address])

        await gems.mint(buyer1.address, saleAmount);
        await gems.connect(buyer1).approve(marketplace.address, saleAmount);

        expect(await gameV2NFT.balanceOf(buyer1.address, 0)).equals(0);
        await marketplace.connect(buyer1).buyListing(0);
        expect(await gameV2NFT.balanceOf(buyer1.address, 0)).equals(1);

        let sellerBalance = saleAmount.mul(95).div(100);
        expect(await marketplace.tokenBalanceOf(seller.address)).equals(sellerBalance);
        let platformFee = saleAmount.mul(5).div(100);
        expect(await marketplace.tokenBalanceOf(owner1.address)).equals(platformFee.mul(30).div(100));
        expect(await marketplace.tokenBalanceOf(owner2.address)).equals(platformFee.mul(35).div(100));
        expect(await marketplace.tokenBalanceOf(owner3.address)).equals(platformFee.mul(35).div(100));

        expect(await gems.balanceOf(seller.address)).equals(0);
        await marketplace.connect(seller).withdrawAllTokens();
        expect(await gems.balanceOf(seller.address)).equals(sellerBalance);
    });
});