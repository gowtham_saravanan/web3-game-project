const {ethers} = require("hardhat");

async function main() {
  const Forwarder = await ethers.getContractFactory("GameMetaForwarder");
  const forwarder = await Forwarder.deploy();
  await forwarder.deployed();
  console.log("Forwarder Address - ", forwarder.address);

  const GameV1NFT = await ethers.getContractFactory("GameV1NFT");
  const gameV1NFT = await GameV1NFT.deploy(forwarder.address);
  await gameV1NFT.deployed();
  console.log("Game V1 NFT Address - ", gameV1NFT.address);

  const GameGems = await ethers.getContractFactory("GameGems");
  const gems = await GameGems.deploy(forwarder.address);
  await gems.deployed();
  console.log("Gem Token Address - ", gems.address);

  const GameV2NFT = await ethers.getContractFactory("GameV2NFT");
  const gameV2NFT = await GameV2NFT.deploy(gameV1NFT.address, gems.address, ethers.utils.parseEther("1"), forwarder.address);
  await gameV2NFT.deployed();
  console.log("Game V2 NFT Address - ", gameV2NFT.address);

  const mOwner1 = "0xeD3836DA347E04e570908E868743e3Ba393f847E";
  const mOwner2 = "0x417b30B7fda2F9e3920bDDb919393e47E84f3af5";
  const mOwner3 = "0x9C30E4ea68A4306B3Bfddd8b3889457C25163479";

  const Marketplace = await ethers.getContractFactory("GameMarketplace");
  const marketplace = await Marketplace.deploy(
      gameV1NFT.address,
      gameV2NFT.address,
      gems.address,
      5,
      [mOwner1, mOwner2, mOwner3],
      [30, 35, 35],
      forwarder.address
  );
  await marketplace.deployed();
  console.log("Marketplace Address - ", marketplace.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
