const {ethers} = require("hardhat");

async function main() {
  const forwarder = await ethers.getContractAt("GameMetaForwarder", "0x3A0a82eD7e4a769c1D0d52B0fcd80D374f58Adb4");
  await forwarder.whiteListContracts([
    '0xa941D0A7bB25d676Cdc9939d2c7F67155402495E',
    '0x907e6aB2dA7150e7184df045447770468045c822',
    '0x8f7d9C60ec33eD1C56B4E7A214668D50D88AB3a4',
    '0x93881d821E1394F94a54986ebfb6EFA2f8e4299D'
  ]);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
