const {ethers} = require("hardhat");

async function main() {
  const gameV1NFT = await ethers.getContractAt("GameV1NFT", "0xa941D0A7bB25d676Cdc9939d2c7F67155402495E");
  await gameV1NFT.safeMint("0x36D098cb0E2c1b931E76ed9Ae00D13474bF69f42");
  await gameV1NFT.safeMint("0x36D098cb0E2c1b931E76ed9Ae00D13474bF69f42")
  await gameV1NFT.safeMint("0x36D098cb0E2c1b931E76ed9Ae00D13474bF69f42")
  await gameV1NFT.safeMint("0x36D098cb0E2c1b931E76ed9Ae00D13474bF69f42")

  const gems = await ethers.getContractAt("GameGems", "0x907e6aB2dA7150e7184df045447770468045c822");
  await gems.mint("0x83463296Fb71caEAD44D4bbA839D637639B0122c", ethers.utils.parseEther("100"));
  await gems.mint("0x36D098cb0E2c1b931E76ed9Ae00D13474bF69f42", ethers.utils.parseEther("100"));
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
