# NFT palace

 - A test project given by a stealth startup to validate ✍️ my smart contract skills. The task was to create a gasless marketplace 🛒. The sellers can list the NFT for sale against a specified no of ERC-20 token called GEM's 💰 and whitelist some buyers. 

 - So the whitelisted buyers will be able to transfer the required GEM's 💰 and buy's the NFT. And other task was to allow user to burn their ERC-721 token to upgrade to ERC-1155 token. 

- Furthermore, gasless ⛽ transaction is also implemented using ERC-2771. The relay is created using NodeJS, and it also tracks the amount of gas spend in a database for analytics.